from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=255, unique=True)
    description = models.TextField(null=True, blank=True)
    isbn = models.CharField(max_length=255, unique=True, default=0)
    year = models.PositiveSmallIntegerField(blank=True, default=0)
    tags = models.ManyToManyField('Tag', blank=True)

    def __str__(self):
        return self.title


class Tag(models.Model):
    title = models.CharField(max_length=32, primary_key=True)
    books = models.ManyToManyField('Book', blank=True)

    def __str__(self):
        return self.title