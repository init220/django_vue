from django.contrib import admin
from library.models import Book, Tag

admin.site.register(Book,)
admin.site.register(Tag,)