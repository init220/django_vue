from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.pagination import PageNumberPagination, CursorPagination

from .models import Book, Tag
from .serializers import UserSerializer, GroupSerializer, BookSerializer, TagSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class MyPagination(CursorPagination):
    page_size = 3
    ordering = 'id'


class BookViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    pagination_class = MyPagination
    permission_classes = [permissions.AllowAny]

    def filter_queryset(self, queryset):
        for k, v in self.request.query_params.items():
            if k == 'cursor':
                continue
            elif k == 'format':
                continue
            queryset = queryset.filter(**{k: v})
        return queryset


class TagViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = [permissions.AllowAny]