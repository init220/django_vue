from django.urls import path, include
from rest_framework import routers

from . import views

app_name = 'library'

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'book', views.BookViewSet)
router.register(r'tag', views.TagViewSet)

urlpatterns = [
    path('', include(router.urls)),
]