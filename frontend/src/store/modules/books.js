import axios from 'axios'
import {host} from '../constants'

export default {
    actions: {
        async removeBook({dispatch}, payload) {
            const {id} = payload
            const response = await axios.delete(
                `${host}/library/book/${id}?format=json`
            )
            if (response.status === 200) {
                alert(JSON.stringify(response.statusText))
            }
            dispatch('fetchBooks')
        },
        async fetchBooks({commit, getters, dispatch}, url = `${host}/library/book/?format=json`) {
            const response = await axios.get(url)
            const data = response.data
            const books = data.results
            const nextPage = data.next
            const prevPage = data.previous

            commit('updateBooks', books)
            commit('updateNextPage', nextPage)
            commit('updatePrevPage', prevPage)
        }
    },
    mutations: {
        updateBooks(state, books) {
            state.books = books
        },
        updateNextPage(state, nextPage) {
            state.nextPage = nextPage
        },
        updatePrevPage(state, prevPage) {
            state.prevPage = prevPage
        }
    },
    state: {

        books: [],
        nextPage: null,
        prevPage: null
    },
    getters: {
        allBooks(state) {
            return state.books
        },
        nextPage(state) {
            return state.nextPage
        },
        prevPage(state) {
            return state.prevPage
        },
    }
}


