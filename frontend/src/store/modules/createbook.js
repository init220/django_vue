import axios from 'axios'
import {host} from '../constants'
import {mapActions} from "vuex";

export default {
    actions: {
        async createBook({dispatch}, payload) {
            const response = await axios.post(
                `${host}/library/book/?format=json`,
                payload
            )
            dispatch('fetchBooks')
        },


    },
    mutations: {},
    state: {},
    getters: {}
}