import Vue from 'vue'
import VueRouter from 'vue-router'
import LibraryWrapper from "../components/library/LibraryWrapper";
import About from "../components/About";

Vue.use(VueRouter)

const routes = [
{
            path: '/about',
            component: About

        },
        {
            path: '/library',
            component: LibraryWrapper

        },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
